Categories:Navigation
License:GPLv3
Web Site:
Source Code:https://github.com/gabm/FancyPlaces
Issue Tracker:https://github.com/gabm/FancyPlaces/issues

Auto Name:Fancy Places
Summary:Store your FancyPlaces
Description:
Store your fancy places (geobookmarks) to find them later..
.

Repo Type:git
Repo:https://github.com/gabm/FancyPlaces

Build:1.0,2
    commit=v1.0-2
    subdir=app
    gradle=yes

Build:1.0.3,3
    commit=v1.0.3
    subdir=app
    gradle=yes

Build:1.1.0,4
    commit=v1.1.0
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.2.0
Current Version Code:5

