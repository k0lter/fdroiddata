Categories:Phone & SMS
License:GPLv3+
Web Site:
Source Code:https://github.com/Nutomic/ensichat
Issue Tracker:https://github.com/Nutomic/ensichat/issues
Bitcoin:1DmU6QVGSKXGXJU1bqmmStPDNsNnYoMJB4

Auto Name:EnsiChat
Summary:Decentralized Instant Messenger
Description:
Instant messanger for Android that is fully decentralized. Messages are sent
directly between devices via Bluetooth, without any central server. A simple
flood-based routing is used for message propagation.
.

Repo Type:git
Repo:https://github.com/Nutomic/ensichat.git

Build:0.1.0,1
    commit=0.1.0
    subdir=app
    gradle=yes

Build:0.1.1,2
    commit=0.1.1
    subdir=app
    gradle=yes

Build:0.1.2,3
    commit=0.1.2
    subdir=app
    gradle=yes

Build:0.1.3,4
    commit=0.1.3
    subdir=app
    gradle=rel

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:0.1.3
Current Version Code:4

