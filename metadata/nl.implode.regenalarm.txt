Categories:Science & Education
License:ISC
Web Site:
Source Code:https://github.com/sanderbaas/rainalarm-android
Issue Tracker:https://github.com/sanderbaas/rainalarm-android/issues
Donate:https://sanderbaas.github.io

Auto Name:Regenalarm
Summary:Two hour rain forecast in The Netherlands
Description:
Graph with precipitation forecast for the next two hours in five minute
intervals. For every location in the Netherlands.
.

Repo Type:git
Repo:https://github.com/sanderbaas/rainalarm-android.git

Build:2.2.2,20202
    disable=licensing issue
    commit=v2.2.2
    srclibs=1:Cordova@3.7.2

Maintainer Notes:
Includes non-commercial library, see https://github.com/sanderbaas/rainalarm-android/issues/1 .
.

#Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:2.2.2
Current Version Code:20202

