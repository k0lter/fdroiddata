Categories:System
License:GPLv3
Web Site:https://github.com/javiersantos/MLManager/blob/HEAD/README.md
Source Code:https://github.com/javiersantos/MLManager
Issue Tracker:https://github.com/javiersantos/MLManager/issues
Changelog:https://github.com/javiersantos/MLManager/blob/HEAD/CHANGELOG.md

Auto Name:ML Manager
Summary:Manage apps
Description:
Modern, easy and customizable app manager with Material Design.
.

Repo Type:git
Repo:https://github.com/javiersantos/MLManager

Build:0.1,2
    commit=c6fac08f21395bfe91f3365553fd76e57e191dee
    subdir=app
    gradle=yes

Build:0.3.1,9
    commit=v0.3.1
    subdir=app
    gradle=yes

Build:0.4.0,10
    commit=v0.4
    subdir=app
    gradle=yes

Build:0.5,11
    commit=v0.5
    subdir=app
    gradle=yes

Build:0.5.1,12
    commit=v0.5.1
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.5.1
Current Version Code:12

